<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">

		<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame
		Remove this if you use the .htaccess -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Códigos de Barras</title>
		<meta name="description" content="">
		<meta name="author" content="Jayr Alencar">

		<meta name="viewport" content="width=device-width; initial-scale=1.0">

		<!-- Replace favicon.ico & apple-touch-icon.png in the root of your domain and delete these references -->
		<link rel="shortcut icon" href="/favicon.ico">
		<link rel="apple-touch-icon" href="/apple-touch-icon.png">
	</head>

	<body>
		<div>
			<header>
				<h1>Usando a biblioteca</h1>
			</header>

			<div>
			    <?php 
			     require_once'lib/boletosPHP.php';
                 
                 $barras = new boletosPHP();
                 
                 $barras->setBarras("00197635800000499000000001243245001124820317");
                 // $barras->setIpte("00190.00009 01142.527033 02576.587188 1 63730000013000");
                 
                 echo $barras->getIpte();
                 echo "<br/>";
                 echo $barras->getNossoNumero();
                 
                 echo "<br/>";
                 echo $barras -> getValorDocumento();
                 echo "<br/>";
                 echo $barras -> getDtVencimento();
                 echo "<br/>";
                 echo $barras -> getFatorVencimento();
                 echo "<br/>";
                 $barras ->desenhaBarras();
                 
			    ?>

			</div>

			<footer>
				<p>
					&copy; Copyright  by CRA-DTI-02
				</p>
			</footer>
		</div>
	</body>
</html>
